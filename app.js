const express = require("express");
const app = express();

const deuxTech = {
    tristan: "Je kiffe Symphony2",
    romain: "J'ai plutôt un profil réseaux",
    valentin: "La catho me manque"
}

app.get("/ping", (req, res) => {
    res.send("Pong!");
});

app.get("/eleves/:prenom", (req, res) => {
    const prenom = req.params.prenom;
    const eleve = deuxTech[prenom];

    if (eleve) {
        res.json(eleve);
    } else {
        res.status(404);
        res.json({ message: "L'élève n'existe pas !"});
    }
})

app.listen(1234, () => console.log("Listening..."));